# MLD
## Informations
*	Les énumérations sont désignées par leurs noms. Le contenu de chaque énumération est indiqué en dessous du MLD.
*	Les relations provenant d'une association ont été nommée à partir des deux relations qu'elles associent. Par exemple, l'association N:M entre Parcelles et EvClimatiques est nommée EvClimatiquesParcelles.
*	Les contraintes simples sont notées directement sur le MLD, le reste est noté en dessous.
*	Une clé artificielle a été crée pour la classe Vins pour deux raisons :
	* Être en 3NF (car sinon dans la classe Parcelles on aurait l'attribut année qui détermine l'attribut millésime. Millésime étant une partie de clé étrangère, il est difficile de décomposer.)
	* Apporter de la simplicité : en effet, la clé primaire de Vins est constituée de 3 attributs et elle est beaucoup utilisée en tant que clé étrangère. Ainsi, avoir un seul attribut en clé étrangère simplifie grandement le modèle.
*   Les teintes sont implémentées soit avec des énumérations, soit avec une classe afin de montrer diverses implémentations possibles. (Il vaut mieux utiliser une table car cela nous permet d'étendre la liste des teintes sans intervenir au niveau du schéma. Cependant, comme nous faisons l'hypothèse qu'il n'y a que 7 uniques teintes et que cela ne peut pas changer, il est possible de faire une énumération.)
## MLD
*   **ModesTaille**(#nom:string, description:text)
*   **ModesGestionSol**(#nom:string, description:text)
*   **TraitementsPhytosanitaires**(#nom:string, #type:string)
*   **TPhytosanitairesParcelles**(#tpNom=>TraitementsPhytosanitaires.nom, #tpType=>TraitementsPhytosanitaires.type, #parcelle=>Parcelles)
*   **Cepages**(#nom:string, description:text)
*   **Parcelles**(#id:int, existence:boolean, annee:int, cepage=>Cepages, modeTaille=>ModesTaille, modeGestionSol=>ModesGestionSol, vin=>Vins)
	* annee, existence NOT NULL
	* NOT(cepage = NULL AND existence='1')
	* NOT(modeTaille = NULL AND existence='1')
	* NOT(modeGestionSol = NULL AND existence='1')
	* NOT(vin = NULL AND existence='1')
*   **CaracteristiquesParcelles**(#codage:string, #annee=>Parcelle, typeSol:string, surface:float, exposition:int)
    * Surface > 0 and NOT NULL
    * 0 <= Exposition <= 10 and NOT NULL
    * typeSol NOT NULL
*   **EvClimatiques**(#type:string, #dateDebut:Date, dateFin:Date)
	* dateFin - dateDebut > 0
*   **EvClimatiquesParcelles**(#parcelle=>Parcelles, #EvcType=>EvClimatiques.type, #EvcDateDebut=>EvClimatiques.dateDebut, intensite:int)
	* 0 <= intensite <= 10 and NOT NULL
*   **Odeurs**(#odeur:string, #vin=>Vins)
*   **TeinteBlanche** (#libelle:string)
*   **Vins**(#id:int, nomDomaine:string, nomCuvee:string, millesime:int, degreAlcool:float, designBouteille:string, prix:float, quantite:int)
	* {nomDomaine, nomCuvee, millesime} UNIQUE NOT NULL
	*  0 <= degreAlcool <= 30 and NOT NULL
	*  prix > 0 and NOT NULL
	*  quantite >= 0 and NOT NULL
	* designBouteille, t NOT NULL
* **VinsRouges**(#vin=>Vins, teinteRouge:TeinteRouge)
* **VinsBlancs**(#vin=>Vins, teinteBlanche=>TeinteBlanche)
    *   teinteBlanche NOT NULL
* **VinsRoses**(#vin=>Vins, teinteRose:TeinteRose)
*   **CircuitsDeVente**(#nom:string, type:string)
	* type NOT NULL
*   **VinsCircuitsDeVente**(#circuitDeVente=>CircuitsDeVente, #vin=>Vins, quantite:int, date:Date)
	* quantite > 0 and NOT NULL
	* date NOT NULL
*   **CriteresGustatifs**(#id:int, #vin=>Vins, douceur:int, acidite:int, tanin:int, corps:int )
	* {douceur, acidite, tanin, corps} UNIQUE NOT NULL
	* 1 <= douceur, acidite, tanin, corps <= 10
	* vin UNIQUE
*   **Aromes**(#arome:string, #criteresGustatifs=>CriteresGustatifs.id, #vin=>CriteresGustatifs.vin)

#### Contenu des énumérations
*   TeinteRouge : {rouge_vermillon, rouge_carmin, grenat, grenat_profond, violace, violace_intense, grenat_tuile}
*   TeinteRose : {lait_de_rose, rose_antique, magnolia, rose_saumon, rose_cerise, framboise, brique}

## Contraintes

#### Associations 1:N
*   PROJECTION(Vins, id) = PROJECTION(Parcelles, vin)
*   PROJECTION(Parcelle, id) = PROJECTION(CaracteristiquesParcelles, (codage, annee))
#### Associations N:M
*   PROJECTION(EvClimatiques, type, dateDebut) = PROJECTION(EvClimatiquesParcelles, type, DateDebut)
#### Héritage par référence (avec classe mère abstraite)
  * PROJECTION(Vins,id) = PROJECTION(VinsRouges,vin) UNION PROJECTION(VinsBlancs,vin) UNION PROJECTION(VinsRoses,vin)

## Choix d'héritage Vins

Voici les différentes propriétés que possède mon héritage sur lesquelles je me suis appuyé afin de choisir l'héritage à appliquer:
-   Classe mère abstraite
-   Héritage exclusif
-   Héritage non complet
-   Associations classe mère

Puisque ma classe mère possède des associations, l'héritage par classes filles est à bannir. Puisque l'héritage est non complet, je ne peux pas non plus faire d'héritage par classe mère.
Ainsi, je suis obligé de faire un héritage par référence.

