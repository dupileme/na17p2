# Note de Clarification
### Contexte

Une exploitation viticole est constituée de nombreuses données et un exploitant veut centraliser ces données au sein d'une base de données afin d'étudier le fonctionnement de l'entreprise et d'avoir une vue d'ensemble des différentes actions et évènements.

### Liste des objets qui devront être gérés dans la base de données
*   Cépages
*   Parcelles
*   Vins (VinsRouges, VinsBlancs, VinsRoses)
*   Critères Gustatifs
*   Circuits De Vente
*   Évènements Climatiques
*   Mode de gestion du sol
*   Mode de taille
*   Traitements Phytosanitaires
***

### Liste des propriétés associées à chaque objet

#### Cépages
- Nom
- Description
- Parcelles

#### Parcelles
-  Codage *(Voir hypothèse 1)*
-  TypeSol *(Terre argileuse, sableuse...)*
-  Surface > 0 *(en m^2)*
-  Exposition au soleil *(sur une échelle de 1 à 10 en fonction de la durée)*
-  Existence *(Voir hypothèse 12)*
-  Année
-  Mode de taille
-  Mode de gestion de sol
-  Traitements Phytosanitaires
-  Cépage
-  Évènements Climatiques
-  Vins

#### Vins
- NomDomaine *(Exp : "Chateau de Lisse")*
- NomCuvee *(Le vigneron choisit ce qu'il veut !)*
- Millésime *(année de récolte des raisins ayant servi à produire le vin)*
- Odeur
- DegréAlcool *(Entre 0 et 30%)*
- DesignBouteille
- Prix > 0 *(pour 1L)*
- Quantité >= 0 (en L)
- Critères Gustatifs
- Circuits de Vente

##### VinsRouges
- Teinte {rouge_vermillon, rouge_carmin, grenat, grenat_profond, violace, violace_intense, grenat_tuile}

##### VinsBlancs
- Teinte {jaune_paille_clair, jaune_clair_reflets_verts, or_vert, jaune_ambre, marron, cuivre, vieil_or}

##### VinsRoses
- Teinte {lait_de_rose, rose_antique, magnolia, rose_saumon, rose_cerise, framboise, brique}

#### Critères Gustatifs
- Douceur *(Voir hypothèse 10)*
- Acidite
- Tanin
- Corps
- Aromes
- Note  *(calculée à partir de la moyenne de (Douceur,Acidite,Tanin,Corps))*

#### Circuits De Vente
- Nom
- Type *(Directe, grossiste, détaillant...)*
- Quantité de vins acheté à une date donnée *(> 0)*

#### Évènements Climatiques
- Parcelles
- Type *(grêle, sécheresse...)*
- Intensité *(sur une échelle de 1 à 10)*
- DateDebut
- DateFin *(DateFin doit être ultérieure à DateDebut)*
- Durée *(calculée à partir de DateDebut et DateFin)*

#### Modes de gestion du sol
- Nom
- Description *(voir hypothèse 11)*
- Parcelles

#### Mode de taille
- Nom
- Description
- Parcelles

#### Traitements Phytosanitaires
- Nom
- Type *(Anti-mildiou, insecticide, herbicide, fongicide...)*
- Parcelles
***

### Liste des utilisateurs (rôles) appelés à modifier et consulter les données
##### Le gérant de l'exploitation viticole
##### Les employés du gérant (Hypothèse 11)
***

### Liste des fonctions que ces utilisateurs pourront effectuer
##### Le gérant de l'exploitation viticole
- Il peut *lire*, *ajouter*, *modifier*, *supprimer* des données sur toutes les tables. Grâce à cela il pourra rentrer les nouvelles données de son exploitation viticole et en modifier/supprimer s'il s'est trompé. Cela lui permettra d'avoir une vue d'ensemble sur son entreprise lui facilitant la prise de décision afin de la développer au mieux.
- Il peut *consulter* l'ensemble des vues.

##### Les employés du gérant
-   *Consulter* Cepages, ModesTaille, ModesGestionSol (pour avoir accès aux descriptions)
-   *Consulter* vFinance
-   *Consulter* vAssurance
-   *Consulter* vQualite
-   *Consulter* vEcologieTP
-   *Consulter* vEcologieDP
-   *Consulter* vCouleur
***

### Liste des vues
* **vFinance** : Vue qui affiche le prix moyen des vins pour un mode de taille, un traitement phytosanitaire et un mode de gestion du sol donné.
* **vAssurance** : Vue qui affiche pour une année donnée le prix des vins dont les parcelles ont été victimes d'un évènement climatique >= 7 ainsi que la quantité de ces vins produits. Il y a aussi le nom et l'intensité de l'évènement climatique ayant affecté la parcelle du vin crée. 
* **vQualité** : Vue qui affiche la note des vins (et leurs moyenne) pour un type de sol, une exposition au soleil, un mode de taille, et un mode de gestion de sol donné.
* **vEcologieTP** : Vue qui affiche, pour une année donnée, la liste des vins (avec leurs notes) pour lesquels les parcelles dont proviennent ces vins ont reçu un traitement phytosanitaire X.
* **vEcologieDP** : Vue qui affiche, pour une année donnée, la liste des vins (avec leurs notes) pour lesquels les parcelles dont proviennent ces vins avaient un mode de gestion désherbage en plein.
* **vCouleur** : Vue qui affiche la note moyenne des vins rouges, blancs et roses afin de savoir quel type de vin l'exploitant réussit le mieux.
***

### Liste des hypothèses faites à partir du sujet
*  **Hypothèse 1** :  Afin de différencier les parcelles, elles possèdent toutes un nom constitué de lettres et de chiffres. Le terrain total est divisé en quadrillage et le nom d'une parcelle dépend de sa position dans le
quadrillage (a la manière de la bataille navale). Les chiffres représentent l'abscisse, les lettres l'ordonnée. (Exp : B8 est la 8ème parcelle en abscisse et la deuxième en ordonnée.)
*   **Hypothèse 2** : « plusieurs vins sont assemblés à partir de différentes parcelles » : à une parcelle, on associe au maximum un vin et un vin est assemblé à partir d'une ou plusieurs parcelles.
*   **Hypothèse 3** : Un seul type de sol et d'exposition est associé à une parcelle
*   **Hypothèse 4** : L'intensité d'un évènement climatique sur une parcelle est la même durant toute sa durée.
*   **Hypothèse 5** : Le prix d'une bouteille de vin dépend en grande partie de sa note.
*   **Hypothèse 6** : Il n'y a que trois couleurs de vins possibles : Rouge, Blanc, Rose. Les rouges et roses peuvent posséder 7 teintes qui leurs sont propres. Les vins blancs possèdent autant de teintes que nécessaires.
*   **Hypothèse 7** : Deux traitements phytosanitaires peuvent avoir le même nom mais pas le même type. C'est pourquoi la clé est {(Nom,Type)}.
*   **Hypothèse 8** : Concernant le mode de gestion du sol, une parcelle peut être :
    * Désherbée en plein : Il y a des vignes et on désherbe le reste.
    * Enherbée et tondue : Il y a des vignes et on enherbe certaines parties puis on les tonds quelques mois plus tard.
    * Cultivée : On s'occupe uniquement des vignes, on ne s'occupe pas de l'herbe qui pousse.
*   **Hypothèse 9** : Le gérant possède quelques employés/stagiaires qui pourront consulter la base de données à souhait. 
*   **Hypothèse 10** : Les propriétés douceur, acidite, tanin, corps de Critères Gustatifs fonctionnent sur une échelle de 1 à 10. Plus la note d'un critère est haute, plus la place de ce critère dans le vin est parfaitement représentée. Ainsi, si l'acidité d'un vin est de 10, cela ne veut pas dire que le vin est très acide mais que le vin à un taux d'acidité parfait (pour créer un vin excellent).
*   **Hypothèse 11** : Les cépages, modes de taille et modes de gestion du sol possèdent une description afin de faciliter la compréhension de ceux-ci par les stagiaires et plus généralement les employés.
*   **Hypothèse 12** :  "Une parcelle peut exister certaines années et ne plus exister" : il s'agit du concept théorique de la parcelle qui n'existe plus. En effet, elle n'est plus utilisée pour faire du raisin. Cependant, elle est toujours bien présente sur le terrain, elle possède donc toujours un codage, un type de sol, une surface et une exposition. Ainsi, on utilise un attribut booléen Existence permettant de savoir si la parcelle est utilisée ou non (en fonction des années). Si Existence = 1 alors la parcelle existe, sinon elle n'existe pas.
*   **Hypothèse 13** : "Chaque vin est vendu, dans différents circuits de vente" : Un vin peut-être temporairement vendu à personne (lorsqu'il est en attente d'achat par des clients)    
*   **Hypothèse 14** : Lorsqu'une parcelle n'existe plus, elle n'a donc pas de Cepages, ModesTaille, ModesGestionSol, TraitementsPhytosanitaires et Vins associés. Elle peut cependant toujours avoir des évènements climatiques associés.