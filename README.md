# Emeric DUPIL - Sujet n°9 - Exploitation viticole

## Livrables 
- Étape 1 à rendre dimanche 24 mai 23h59  : NDC, MCD
- Étape 2 à rendre dimanche 31 mai 23h59  : NDC, MCD, MLD, SQL CREATE et INSERT
- Étape 3 à rendre dimanche 7 juin 23h59  : NDC, MCD, MLD, SQL CREATE et INSERT, SQL SELECT, Complément : PostgreSQL/JSON (NA17, NF17)
- Étape 4 à rendre dimanche 14 juin 23h59 : Révision complète du projet (v2)

### Contexte
* Une exploitation viticole est constituée de nombreuses données et un exploitant veut centraliser ces données au sein d'une base de données afin d'étudier le fonctionnement de l'entreprise et d'avoir une vue d'ensemble des différents actions et événements.

### Données
* La catégorisation de ces données est à titre indicatif, et ne doit pas contraindre le travail de modélisation.

### Caractéristiques de la parcelle
* Les vignes sont plantées sur des parcelles, il existe des données caractéristiques du sol (type de sol, par exemple terre argileuse), de surface, d'exposition.
* Sur chaque parcelle est plantée un seul cépage, qui peut-être planté sur plusieurs parcelle.
* Une parcelle peut exister certaines années et ne plus exister (en cas de plantation ou d'arrachage).

### Caractéristiques du vin
* Chaque années plusieurs vins sont assemblés à partir de différentes parcelles, dans des proportions qui dépendent du choix de l'exploitant.
* Chaque vin est analysé et dégusté, des critères qualitatifs sont caractéristiques des vins.
* Chaque vin est vendu, dans différents circuits de vente (directe, grossiste, détaillants...), les chiffres de ventes sont connus ainsi que les prix.

### Caractéristiques des événements climatiques
* Chaque année des événements climatiques surviennent, ils peuvent être de plusieurs types (grêle, sécheresse...). Un événement climatique peut toucher toutes les parcelles ou un sous-ensembles, il peut toucher avec une intensité différente chaque parcelle.

### Caractéristiques du mode de culture
* Certaines parcelles sont désherbées en plein, enherbées et tondues, ou cultivées, le mode de gestion du sol peut changer chaque année.
* Différents modes de tailles sont utilisés sur les différentes parcelles, iceux peuvent varier en fonction des années.
* Différents traitement phytosanitaires sont effectuées, chaque année, sur tout ou partie des parcelles.

### Objectif financier
* L'exploitant veut pouvoir connaître l'influence sur le prix du vin des modes de culture, et l'influence sur le prix des vin des événements climatiques pour demander un dédommagement à son assurance couvrant de tel dommages.

### Objectif qualité
* Dans un objectif qualitatif, l'exploitant veut augmenter la qualité de ses vin, et il veut connaître l'influence des différents assemblages de terroir et l'influence de son mode de culture sur la qualité des vins.

### Objectif écologique
* Pour minimiser l'impact écologique de sa culture l'exploitant veut connaître l'influence sur la qualité des vins du désherbage en plein, et des traitements phytosanitaires, et le moyen de les réduire à qualité constante.