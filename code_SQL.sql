-- CREATION DES ENUMS ---

CREATE TYPE public."TeinteRose" AS ENUM (
    'lait_de_rose',
    'rose_antique',
    'magnolia',
    'rose_saumon',
    'rose_cerise',
    'framboise',
    'brique'
);

CREATE TYPE public."TeinteRouge" AS ENUM (
    'rouge_vermillon',
    'rouge_carmin',
    'grenat',
    'grenat_profond',
    'violace',
    'violace_intense',
    'grenat_tuile'
);

-- CREATION DES TABLES ---

CREATE TABLE IF NOT EXISTS public."ModesGestionSol"
(
    nom character varying(50) NOT NULL,
    description text,
    CONSTRAINT "ModesGestionSol _pkey" PRIMARY KEY (nom)
);

CREATE TABLE IF NOT EXISTS public."ModesTailles"
(
    nom character varying(50) NOT NULL,
    description text,
    CONSTRAINT "ModesTailles_pkey" PRIMARY KEY (nom)
);

CREATE TABLE IF NOT EXISTS public."Cepages"
(
    nom character varying(50) NOT NULL,
    description text,
    CONSTRAINT "Cepages_pkey" PRIMARY KEY (nom)
);

CREATE TABLE IF NOT EXISTS public."CircuitsDeVente"
(
    nom character varying(50) NOT NULL,
    type character varying(50) NOT NULL,
    CONSTRAINT "CircuitsDeVente_pkey" PRIMARY KEY (nom)
);

CREATE TABLE IF NOT EXISTS public."TeinteBlanche"
(
    libelle character varying(50) NOT NULL,
    CONSTRAINT "TeinteBlanche_pkey" PRIMARY KEY (libelle)
);

CREATE TABLE IF NOT EXISTS public."Vins"
(
    id SERIAL NOT NULL,
    "nomDomaine" character varying(50) NOT NULL,
    "nomCuvee" character varying(50) NOT NULL,
    millesime smallint NOT NULL,
    degrealcool numeric(4,1) NOT NULL,
    "designBouteille" character varying(50) NOT NULL,
    prix numeric(8,2) NOT NULL,
    quantite smallint NOT NULL,
    Odeur JSON NOT NULL,
    CriteresGustatifs JSON NOT NULL,
    CONSTRAINT "Vins_pkey" PRIMARY KEY (id),
    CONSTRAINT "VinND_NC_MI" UNIQUE ("nomDomaine", "nomCuvee", millesime),
    CONSTRAINT "QuantitePositive" CHECK (quantite >= 0) NOT VALID,
    CONSTRAINT "degreAlcoolMax" CHECK (degrealcool >= 0::numeric AND degrealcool <= 30::numeric) NOT VALID,
    CONSTRAINT "PrixPositif" CHECK (prix > 0::numeric) NOT VALID
);

CREATE TABLE IF NOT EXISTS public."VinsRouge"
(
    vin integer NOT NULL,
    teinterouge "TeinteRouge" NOT NULL,
    CONSTRAINT "VinsRouge_pkey" PRIMARY KEY (vin),
    CONSTRAINT "VinsRouge_vin_fkey" FOREIGN KEY (vin) REFERENCES public."Vins" (id)
);

CREATE TABLE IF NOT EXISTS public."VinsRoses"
(
    vin integer NOT NULL,
    teinterose "TeinteRose" NOT NULL,
    CONSTRAINT "VinsRoses_pkey" PRIMARY KEY (vin),
    CONSTRAINT "VinsRoses_vin_fkey" FOREIGN KEY (vin) REFERENCES public."Vins" (id)
);

CREATE TABLE IF NOT EXISTS public."VinsBlancs"
(
    vin integer NOT NULL,
    teinteblanche character varying(50) NOT NULL,
    CONSTRAINT "VinsBlancs_pkey" PRIMARY KEY (vin),
    CONSTRAINT "VinsBlancs_vin_fkey" FOREIGN KEY (vin) REFERENCES public."Vins" (id),
    CONSTRAINT "VinsBlancs_teinteblanche_fkey" FOREIGN KEY (teinteblanche) REFERENCES public."TeinteBlanche" (libelle)
);

CREATE TABLE IF NOT EXISTS public."EvClimatiques"
(
    type character varying(50) NOT NULL,
    datedebut date NOT NULL,
    datefin date,
    CONSTRAINT "EvClimatiques_pkey" PRIMARY KEY (type, datedebut),
    CONSTRAINT "dateCorrecte" CHECK (datefin > datedebut)
);

CREATE TABLE IF NOT EXISTS public."Parcelles"
(
    id SERIAL NOT NULL,
    existence boolean NOT NULL,
    annee smallint NOT NULL,
    cepage character varying(50),
    modetaille character varying(50),
    modegestionsol character varying(50),
    vin integer,
    CONSTRAINT "Parcelles_pkey" PRIMARY KEY (id),
    CONSTRAINT "Parcelles_cepage_fkey" FOREIGN KEY (cepage)
        REFERENCES public."Cepages" (nom),
    CONSTRAINT "Parcelles_modeGestionSol_fkey" FOREIGN KEY (modegestionsol)
        REFERENCES public."ModesGestionSol" (nom),
    CONSTRAINT "Parcelles_modeTaille_fkey" FOREIGN KEY (modetaille)
        REFERENCES public."ModesTailles" (nom),
    CONSTRAINT "Parcelles_vin_fkey" FOREIGN KEY (vin)
        REFERENCES public."Vins" (id),
    CONSTRAINT existence1 CHECK (NOT (cepage::text = NULL::text AND existence = true) AND NOT (modetaille::text = NULL::text AND existence = true) AND NOT (modegestionsol::text = NULL::text AND existence = true) AND NOT (vin = NULL::integer AND existence = true))
);

CREATE TABLE IF NOT EXISTS public."CaracteristiquesParcelles"
(
    codage character varying(8) NOT NULL,
    annee integer NOT NULL,
    "typeSol" character varying(50) NOT NULL,
    surface numeric(10,2) NOT NULL,
    exposition smallint NOT NULL,
    CONSTRAINT "CaracteristiquesParcelles_pkey" PRIMARY KEY (codage, annee),
    CONSTRAINT "CaracteristiquesParcelles_Parcelles_fkey" FOREIGN KEY (annee)
      REFERENCES public."Parcelles" (id),
    CONSTRAINT "echelleExposition" CHECK (exposition >= 0 AND exposition <= 10),
    CONSTRAINT "SurfacePositive" CHECK (surface > 0::numeric)
);

CREATE TABLE IF NOT EXISTS public."EvClimatiquesParcelles"
(
    parcelle integer NOT NULL,
    "evcType" character varying(50) NOT NULL,
    "evcDateDebut" date NOT NULL,
    intensite smallint NOT NULL,
    CONSTRAINT "EvClimatiquesParcelles_pkey" PRIMARY KEY (parcelle, "evcType", "evcDateDebut"),
    CONSTRAINT "EvClimatiquesParcelles_evcType_evcDateDebut_fkey" FOREIGN KEY ("evcDateDebut", "evcType")
        REFERENCES public."EvClimatiques" (datedebut, type),
    CONSTRAINT "EvClimatiquesParcelles_parcelle_fkey" FOREIGN KEY (parcelle)
        REFERENCES public."Parcelles" (id),
    CONSTRAINT "echelleIntensite" CHECK (intensite >= 0 AND intensite <= 10)
);

CREATE TABLE IF NOT EXISTS public."TraitementsPhytosanitaires"
(
    nom character varying(50) NOT NULL,
    type character varying(50) NOT NULL,
    CONSTRAINT "TraitementsPhytosanitaires_pkey" PRIMARY KEY (nom, type)
);

CREATE TABLE IF NOT EXISTS public."TPhytosanitairesParcelles"
(
    "tpNom" character varying(50) NOT NULL,
    "tpType" character varying(50) NOT NULL,
    parcelle integer NOT NULL,
    CONSTRAINT "TPhytosanitairesParcelles_pkey" PRIMARY KEY ("tpNom", "tpType", parcelle),
    CONSTRAINT "TPhytosanitairesParcelles_parcelle_fkey" FOREIGN KEY (parcelle)
        REFERENCES public."Parcelles" (id),
    CONSTRAINT "TPhytosanitairesParcelles_tpNom_tpType_fkey" FOREIGN KEY ("tpNom", "tpType")
        REFERENCES public."TraitementsPhytosanitaires" (nom, type)
);

CREATE TABLE IF NOT EXISTS public."VinsCircuitsDeVente"
(
    "circuitDeVente" character varying(50) NOT NULL,
    vin integer NOT NULL,
    quantite integer NOT NULL,
    date date NOT NULL,
    CONSTRAINT "VinsCircuitsDeVente_pkey" PRIMARY KEY ("circuitDeVente", vin),
    CONSTRAINT "VinsCircuitsDeVente_circuitDeVente_fkey" FOREIGN KEY ("circuitDeVente")
        REFERENCES public."CircuitsDeVente" (nom),
    CONSTRAINT "VinsCircuitsDeVente_vin_fkey" FOREIGN KEY (vin)
        REFERENCES public."Vins" (id),
    CONSTRAINT "quantitePositive" CHECK (quantite > 0)
);

CREATE TABLE IF NOT EXISTS public."user" (
  email VARCHAR(100) PRIMARY KEY,
  mdp VARCHAR(100) NOT NULL
);

  --- Les donnees ---
INSERT INTO public."user" VALUES('gerant@gmail.com', 'motdepasse');

INSERT INTO public."Vins" VALUES (4, 'Chateau_Mrik', 'Azzag_Vie', 2018, 17.0, 'Ovale', 12.99, 120, '["menthe"]', '{"douceur":"1","acidite":"2","tanin":"2","corps":"2","aromes":["citronnelle"]}');
INSERT INTO public."Vins" VALUES (1, 'Chateau_Mrik', 'UTC_Vie', 2019, 17.4, 'Basique', 28.22, 100, '["jacinthe"]', '{"douceur":"6","acidite":"5","tanin":"4","corps":"2","aromes":["rose","grenade"]}');
INSERT INTO public."Vins" VALUES (2, 'Chateau_Mrik', 'Aller_les_bleu', 2018, 22.3, 'Coupe_du_monde', 199.99, 29, '["cassis","poire","venaison"]', '{"douceur":"7","acidite":"8","tanin":"8","corps":"10","aromes":["coriandre","biryani","fleur_oranger"]}');
INSERT INTO public."Vins" VALUES (3, 'Chateau_Mrik', 'La_Mikasa', 2019, 19.8, 'Basique', 45.30, 50, '["silex","rose"]','{"douceur":"6","acidite":"5","tanin":"5","corps":"5","aromes":["citronnelle", "kaki"]}');
INSERT INTO public."Vins" VALUES (5, 'Chateau_Mrik', 'Sleepless', 2019, 12.3, 'Basique', 8.99, 250, '["verveine","menthe"]', '{"douceur":"1","acidite":"1","tanin":"1","corps":"2", "aromes":["kiwi"]}');
INSERT INTO public."Vins" VALUES (6, 'Chateau_Mrik', 'Ali', 2018, 24.1, 'Soleil', 3600.99, 24, '["iris"]', '{"douceur":"10","acidite":"10","tanin":"10","corps":"10", "aromes":["citron_vert","poisson_pane"]}');

INSERT INTO public."TeinteBlanche" VALUES ('jaune_paille_clair');
INSERT INTO public."TeinteBlanche" VALUES ('jaune_clair_reflets_verts');
INSERT INTO public."TeinteBlanche" VALUES ('or_vert');
INSERT INTO public."TeinteBlanche" VALUES ('jaune_ambre');
INSERT INTO public."TeinteBlanche" VALUES ('marron');
INSERT INTO public."TeinteBlanche" VALUES ('cuivre');
INSERT INTO public."TeinteBlanche" VALUES ('vieil_or');

INSERT INTO public."VinsRouge" VALUES (4, 'violace_intense');
INSERT INTO public."VinsRouge" VALUES (1, 'rouge_vermillon');
INSERT INTO public."VinsRouge" VALUES (5, 'grenat_tuile');

INSERT INTO public."VinsRoses" VALUES (2, 'lait_de_rose');
INSERT INTO public."VinsRoses" VALUES (6, 'rose_antique');

INSERT INTO public."VinsBlancs" VALUES (3, 'jaune_paille_clair');

INSERT INTO public."Cepages" VALUES ('Cabernet_Franc', 'Cépage rouge');
INSERT INTO public."Cepages" VALUES ('Grenache', 'Cépage rouge');
INSERT INTO public."Cepages" VALUES ('Aligoté', 'Cépage blanc');
INSERT INTO public."Cepages" VALUES ('Chardonnay', 'Cépage blanc');
INSERT INTO public."Cepages" VALUES ('Muscadet', 'Cépage blanc');
INSERT INTO public."Cepages" VALUES ('Syrah', 'Cépage rouge');
INSERT INTO public."Cepages" VALUES ('Merlot', 'Cépage rouge');
INSERT INTO public."Cepages" VALUES ('Auxerrois', 'Cépage blanc');

INSERT INTO public."ModesGestionSol" VALUES ('Cultivee', 'On s''occupe uniquement des vignes, on ne s''occupe pas de l''herbe qui pousse.');
INSERT INTO public."ModesGestionSol" VALUES ('Desherbe_en_plein', 'Il y a des vignes et on désherbe le reste.');
INSERT INTO public."ModesGestionSol" VALUES ('Enherbee_et_tondue', 'Il y a des vignes et on enherbe certaines parties puis on les tonds quelques mois plus tard.');

INSERT INTO public."ModesTailles" VALUES ('gobelet', 'Avec cette taille, seulement 3 à 5 bras (branches) sont gardés sur le pied de vigne. Les branches se terminent par un courson de 3 ou 4 bourgeons.');
INSERT INTO public."ModesTailles" VALUES ('cordon_en_Royat', 'Cette taille se rapproche légèrement de la taille en gobelet car elle peut être courte mais a la particularité de pouvoir aussi être haute. Pour cette taille il faut garder seulement 1 ou 2 bras avec chacun 3 ou 4 coursons à 2 bourgeons. ');
INSERT INTO public."ModesTailles" VALUES ('guyot', 'Considérée comme la plus productive,  la taille guyot ne garde que les bourgeons fructifères (qui sont fertiles). Pour cette taille on préfère la qualité des grappes plutôt que la quantité produite.');
INSERT INTO public."ModesTailles" VALUES ('lyre', 'C’est une taille haute, deux bras perpendiculaires portent les coursons. Cette méthode est faite pour éviter les zones d’ombre ainsi qu’à optimiser l’ensoleillement et la ventilation de la vigne.');
INSERT INTO public."ModesTailles" VALUES ('mecanique_de_precision', 'Cette taille est très courte avec un ou deux bourgeons et vise à créer une ligne de taille renouvelée chaque années mais à la même hauteur. Plus la ligne de taille est rectiligne, plus la taille mécanique sera facile à effectuer.');

INSERT INTO public."Parcelles" VALUES (2, true, 2019, 'Cabernet_Franc', 'cordon_en_Royat', 'Desherbe_en_plein', 1);
INSERT INTO public."Parcelles" VALUES (3, true, 2018, 'Grenache', 'cordon_en_Royat', 'Desherbe_en_plein', 1);
INSERT INTO public."Parcelles" VALUES (4, true, 2018, 'Syrah', 'gobelet', 'Cultivee', 2);
INSERT INTO public."Parcelles" VALUES (5, true, 2018, 'Auxerrois', 'cordon_en_Royat', 'Desherbe_en_plein', 3);
INSERT INTO public."Parcelles" VALUES (6, true, 2018, 'Merlot', 'cordon_en_Royat', 'Cultivee', 4);
INSERT INTO public."Parcelles" VALUES (7, true, 2019, 'Merlot', 'guyot', 'Enherbee_et_tondue', 5);
INSERT INTO public."Parcelles" VALUES (8, true, 2018, 'Syrah', 'gobelet', 'Cultivee', 6);
INSERT INTO public."Parcelles" VALUES (9, false, 2018, NULL, NULL, NULL, NULL);

INSERT INTO public."CaracteristiquesParcelles" VALUES ('A1', 2, 'argilo-calcaires', 60.00, 8);
INSERT INTO public."CaracteristiquesParcelles" VALUES ('A2', 3, 'argilo-calcaires', 120.00, 6);
INSERT INTO public."CaracteristiquesParcelles" VALUES ('B1', 4, 'argilo-calcaires', 57.00, 6);
INSERT INTO public."CaracteristiquesParcelles" VALUES ('B2', 5, 'argilo-calcaires', 26.00, 2);
INSERT INTO public."CaracteristiquesParcelles" VALUES ('A1', 6, 'argilo-calcaires', 60.00, 8);
INSERT INTO public."CaracteristiquesParcelles" VALUES ('A2', 7, 'argilo-calcaires', 120.00, 6);
INSERT INTO public."CaracteristiquesParcelles" VALUES ('B1', 8, 'argilo-calcaires', 57.00, 6);
INSERT INTO public."CaracteristiquesParcelles" VALUES ('B2', 9, 'argilo-calcaires', 26.00, 2);

INSERT INTO public."CircuitsDeVente" VALUES ('M_Dupont', 'Direct');
INSERT INTO public."CircuitsDeVente" VALUES ('Le_Vin_Pour_Tous', 'Grossiste');
INSERT INTO public."CircuitsDeVente" VALUES ('Marche_Aubervilliers', 'Detaillant');
INSERT INTO public."CircuitsDeVente" VALUES ('Fruit_vie', 'Detaillant');

INSERT INTO public."EvClimatiques" VALUES ('Grele', '2018-01-01', '2018-01-24');
INSERT INTO public."EvClimatiques" VALUES ('Forte_pluie', '2018-02-02', '2018-02-14');
INSERT INTO public."EvClimatiques" VALUES ('Canicule', '2018-06-03', '2018-06-28');
INSERT INTO public."EvClimatiques" VALUES ('Canicule', '2019-07-03', '2019-07-12');

INSERT INTO public."EvClimatiquesParcelles" VALUES (5, 'Canicule', '2018-06-03', 8);
INSERT INTO public."EvClimatiquesParcelles" VALUES (3, 'Canicule', '2018-06-03', 3);
INSERT INTO public."EvClimatiquesParcelles" VALUES (3, 'Forte_pluie', '2018-02-02', 2);
INSERT INTO public."EvClimatiquesParcelles" VALUES (3, 'Grele', '2018-01-01', 6);
INSERT INTO public."EvClimatiquesParcelles" VALUES (5, 'Grele', '2018-01-01', 7);

INSERT INTO public."TraitementsPhytosanitaires" VALUES ('Luna_Sensation', 'Fongicide');
INSERT INTO public."TraitementsPhytosanitaires" VALUES ('Cassiopee', 'Fongicide_systemique');
INSERT INTO public."TraitementsPhytosanitaires" VALUES ('Chaoline', 'Fongicide_preventif');
INSERT INTO public."TraitementsPhytosanitaires" VALUES ('Falarik_DG', 'Fongicide_systemique');
INSERT INTO public."TraitementsPhytosanitaires" VALUES ('Decis_Protech', 'Insecticide');
INSERT INTO public."TraitementsPhytosanitaires" VALUES ('Kameto_Protect', 'Insecticide');

INSERT INTO public."TPhytosanitairesParcelles" VALUES ('Cassiopee', 'Fongicide_systemique', 2);
INSERT INTO public."TPhytosanitairesParcelles" VALUES ('Chaoline', 'Fongicide_preventif', 3);
INSERT INTO public."TPhytosanitairesParcelles" VALUES ('Kameto_Protect', 'Insecticide', 2);
INSERT INTO public."TPhytosanitairesParcelles" VALUES ('Luna_Sensation', 'Fongicide', 4);
INSERT INTO public."TPhytosanitairesParcelles" VALUES ('Decis_Protech', 'Insecticide', 5);
INSERT INTO public."TPhytosanitairesParcelles" VALUES ('Kameto_Protect', 'Insecticide', 6);

INSERT INTO public."VinsCircuitsDeVente" VALUES ('Le_Vin_Pour_Tous', 4, 80, '2019-01-24');
INSERT INTO public."VinsCircuitsDeVente" VALUES ('Fruit_vie', 1, 8, '2020-06-03');
INSERT INTO public."VinsCircuitsDeVente" VALUES ('Le_Vin_Pour_Tous', 2, 5, '2019-03-31');
INSERT INTO public."VinsCircuitsDeVente" VALUES ('M_Dupont', 5, 2, '2020-12-22');


--- Les Vues ---

CREATE VIEW vFinance (PrixMoyen, ModeGestionSol, ModeTaille, TP_nom, TP_type) AS
SELECT
  ROUND(AVG(prix)), modegestionsol, modetaille, public."TPhytosanitairesParcelles"."tpNom", public."TPhytosanitairesParcelles"."tpType"
FROM
  public."Vins" JOIN public."Parcelles" ON public."Vins".id = public."Parcelles".vin
  JOIN public."CaracteristiquesParcelles" ON public."CaracteristiquesParcelles".annee = public."Parcelles".id
  LEFT JOIN public."TPhytosanitairesParcelles" ON public."TPhytosanitairesParcelles".parcelle = public."Parcelles".id
GROUP BY
  modegestionsol, modetaille, public."TPhytosanitairesParcelles"."tpNom", public."TPhytosanitairesParcelles"."tpType"
ORDER BY ROUND(AVG(prix)) DESC;


CREATE VIEW vAssurance (Annee, Id_Vin, Prix, Quantite, Type_Ev, Intensite_Ev) AS
SELECT
  public."Parcelles".annee, public."Vins".id, prix, quantite, "EvClimatiquesParcelles"."evcType","EvClimatiquesParcelles".intensite
FROM
  public."Vins" JOIN public."Parcelles" ON public."Vins".id = public."Parcelles".vin
  JOIN public."EvClimatiquesParcelles" ON public."EvClimatiquesParcelles".parcelle = public."Parcelles".id
WHERE
  public."EvClimatiquesParcelles".intensite >= 7
GROUP BY
  public."Parcelles".annee, public."Vins".id, prix, quantite, "EvClimatiquesParcelles"."evcType", "EvClimatiquesParcelles".intensite
ORDER BY public."Parcelles".annee DESC;


CREATE OR REPLACE FUNCTION calculer_note(dou integer, aci integer, tan integer, cor integer)
RETURNS float as $moyenne$
DECLARE
  res float = 0;
BEGIN
  res = (dou + aci + tan + cor)/4;
  RETURN res;
END;
$moyenne$ LANGUAGE plpgsql;

CREATE VIEW vQualite (Vin, Note, TypeSol, Exposition, ModeTaille, ModeGestionSol, Douceur, Acidite, Tanin, Corps) AS
SELECT public."Vins".id, calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)),
       "typeSol", exposition, modetaille, modegestionsol, CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)
FROM
  public."Vins" JOIN public."Parcelles" ON public."Vins".id = public."Parcelles".vin
  JOIN public."CaracteristiquesParcelles" ON public."CaracteristiquesParcelles".annee = public."Parcelles".id
ORDER BY calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)) DESC;


CREATE VIEW vCouleur (Note, Rouge, Rose, Blanc) AS
SELECT calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)),
       teinterouge, teinterose, teinteblanche
FROM
  public."Vins" LEFT JOIN public."VinsRouge" ON public."Vins".id = public."VinsRouge".vin
  LEFT JOIN public."VinsRoses" ON public."Vins".id = public."VinsRoses".vin
  LEFT JOIN public."VinsBlancs" ON public."Vins".id = public."VinsBlancs".vin
ORDER BY
calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)) DESC;


CREATE VIEW vEcologieTP(Annee, Vin, Note, Parcelle, Tp_Nom, Tp_Type) AS
SELECT
  public."Parcelles".annee, public."Vins".id, calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)),
  codage,  public."TPhytosanitairesParcelles"."tpNom", public."TPhytosanitairesParcelles"."tpType"
FROM
  public."Vins" JOIN public."Parcelles" ON public."Vins".id = public."Parcelles".vin
  JOIN public."CaracteristiquesParcelles" ON public."CaracteristiquesParcelles".annee = public."Parcelles".id
  JOIN public."TPhytosanitairesParcelles" ON public."TPhytosanitairesParcelles".parcelle = public."Parcelles".id
ORDER BY
  calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)) DESC;


CREATE VIEW vEcologieDP(Annee, Vin, Note, Parcelle, ModeGestionSol) AS
SELECT
    public."Parcelles".annee, public."Vins".id, calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)),
    codage, modegestionsol
FROM
    public."Vins" JOIN public."Parcelles" ON public."Vins".id = public."Parcelles".vin
    JOIN public."CaracteristiquesParcelles" ON public."CaracteristiquesParcelles".annee = public."Parcelles".id
WHERE
    modegestionsol='Desherbe_en_plein'
ORDER BY
    calculer_note(CAST(public."Vins".criteresGustatifs->>'douceur' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'acidite' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'tanin' AS INTEGER), CAST(public."Vins".criteresGustatifs->>'corps' AS INTEGER)) DESC;


--- Le gérant de l'exploitation viticole peut lire, ajouter, modifier et supprimer des données sur toutes les tables.
CREATE USER gerant with PASSWORD 'gerant';
GRANT SELECT, INSERT, DELETE, UPDATE
ON public."ModesGestionSol", public."ModesTailles", public."Cepages", public."CircuitsDeVente", public."TeinteBlanche", public."Vins", public."VinsRouge", public."VinsRoses",
public."VinsBlancs", public."EvClimatiques", public."Parcelles", public."CaracteristiquesParcelles", public."EvClimatiquesParcelles",
public."TraitementsPhytosanitaires", public."TPhytosanitairesParcelles", public."VinsCircuitsDeVente"
TO gerant;

--- Les employés du gérant peuvent consulter la base de données à souhait.
CREATE USER visiteur WITH PASSWORD 'visiteur';
GRANT SELECT
ON public."ModesGestionSol", public."ModesTailles", public."Cepages", public."Vins", public."user"
TO visiteur;

--- Tout le monde peut consulter les Vues
GRANT SELECT
ON vFinance, vAssurance, vQualite, vCouleur, vEcologieTP, vEcologieDP
TO gerant, visiteur;
