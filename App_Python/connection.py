import psycopg2

## Fonction permettant à un utilisateur de se connecter au site. ##
def authentification(cur) :
    email=''
    mdp=''
    print("Entrez votre email :")
    email=input()
    print("Entrez votre mot de passe:")
    mdp =input()
    user = "SELECT * FROM public.\"user\" WHERE email='%s' AND mdp='%s';" % (email, mdp)
    cur.execute(user)
    row = cur.fetchall()
    if row is None :
        print("Identifiants incorrects.")
        return False
    else :
        print("Vous êtes désormais connecté !")
        return True


## Fonction permettant d'afficher les parcelles. Seuls les utilisateurs connectés (USER GERANT dans la bdd) peuvent le faire.
def afficherParcelles(cur,id):
    listParcelles = "SELECT * from  public.\"Parcelles\" WHERE id=%i" %(id)
    cur.execute(listParcelles)
    row = cur.fetchall()
    for i in row:
        print("Existence : ",row[0][1])
        print("Année :",row[0][2])
        print("Cépage :",row[0][3])
        print("ModeTaille :",row[0][4])
        print("ModeGestionSol :",row[0][5])
        print("Vin produit: ",row[0][6])
        print("\n\n")

## Fonction permettant d'afficher un vin en fonction de son id. Tous le monde peut l'utiliser. ##
def listeVins(cur, id):
    listVins = "SELECT * from  public.\"Vins\" WHERE id=%i" %(id)
    cur.execute(listVins)
    row = cur.fetchall()
    for i in row:
        print("Nom de domaine : ",row[0][1])
        print("Nom de cuvée :",row[0][2])
        print("Millésime :",row[0][3])
        print("Degré d'alcool :",row[0][4])
        print("Desin bouteille :",row[0][5])
        print("Prix : ",row[0][6])
        print("Quantité :",row[0][7])
        print("Odeurs :",row[0][8])
        print("Critères gustatifs :", row[0][9])
        print("\n\n")

## Fonction affichant la vue qualité. Tout le monde peut l'utiliser. ##
def vQualite(cur):
    vQualite = "SELECT * from vQualite"
    cur.execute(vQualite)
    row = cur.fetchall()
    for i in range(0, len(row)):
        print("Vin:",row[i][0], "Note:", row[i][1], "Typesol:",row[i][2], "Exposition:",row[i][3],"ModeTaille :",row[i][4], "ModeGestionSol:",row[i][5], "\n")


## L'application ##
def app():
# CONNEXION #
    authentifie = False
    host='localhost'
    dbname='na17_DUPIL'
    user='visiteur'
    password='visiteur'
    print('Connecting to the PostgreSQL database...')
    conn = psycopg2.connect("host='%s' dbname='%s' user='%s' password='%s'"%(host,dbname,user,password))
    try:
        cur = conn.cursor()

# MENU TEXTUEL #
        menu=0
        vin=0
        print("Bienvenue visiteur ! ")
        while menu != 4 :
            print("Que voulez vous faire ?")
            print("1 - Afficher un vin")
            print("2 - Afficher la vue qualité")
            if authentifie == False :
                print("3 - Authentification")
            else :
                print("3 - Afficher Parcelle")
            print("4 - Quitter")
            menu=int(input())
            if menu == 1 :
                print("Quel vin voulez-vous afficher? (id) \n")
                vin=int(input())
                listeVins(cur,vin)
            elif menu == 2 :
                vQualite(cur)
            elif menu == 3 :
                if authentifie == False :
                    if authentification(cur) == True :
                        user='gerant'
                        password='gerant'
                        ## Si on est authentifié, on change de USER dans la base de données ! ##
                        ## Donc, on se déconnecte de la bdd et on se reconnecte avec le bon user ! ##
                        cur.close()
                        conn.close()
                        conn = psycopg2.connect("host='%s' dbname='%s' user='%s' password='%s'"%(host,dbname,user,password))
                        cur = conn.cursor()
                        authentifie = True
                else :
                    print("Quelle parcelle voulez-vous afficher? (id) \n")
                    id=int(input())
                    afficherParcelles(cur,id)
            elif menu > 4 :
                print("Veuillez rentrer une valeur correcte. \n")

	    ## Déconnexion ##
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')


if __name__ == '__main__':
    app()
