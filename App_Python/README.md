* Afin que l'application python fonctionne, la base de données doit s'appeler "na17_DUPIL".
* Vous pouvez si vous le souhaiter, changer le nom en modifiant la ligne 67 du fichier connection.py :
    *     dbname='na17_DUPIL' -> dbname='le_nouveau_nom'